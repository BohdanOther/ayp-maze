﻿namespace GameService.Common
{
    public class RemoteContext
    {
        public int RemoteX { get; set; }
        public int RemoteY { get; set; }
        public bool RemoteFinishReached { get; set; }
        //
        //
        //
        public int LocalX { get; set; }
        public int LocalY { get; set; }
        public bool LocalFinishReached { get; set; }
    }
}
