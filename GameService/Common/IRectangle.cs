﻿namespace GameService.Common
{
    interface IRectangle
    {
        float PosX { set; get; }
        float PosY { set; get; }
        float Width { set; get; }
        float Height { set; get; }
    }
}
