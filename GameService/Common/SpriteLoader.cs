﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace GameService.Common
{
    public enum Sprite
    {
        Player,

        Wall,
        Floor,
   
        Start,
        Finish,

        Catcher
    }

    internal static class SpriteLoader
    {
        private static Dictionary<Sprite, Bitmap> tileSet = new Dictionary<Sprite, Bitmap>();

        public static void LoadSprites(string theme)
        {
            // check if specified theme folder exists
            string path = GameContext.ResourcesPath + @"Themes\" + theme + @"\";

            // if not - using default theme
            if (!Directory.Exists(path))
            {
                path = GameContext.ResourcesPath + @"\Themes\default\";
            }

            // get all file pathes in theme folder
            string[] spritesPath = Directory.GetFiles(path);

            // get all file extensions
            Dictionary<string, string> spriteExtension = new Dictionary<string, string>();
            foreach (string s in spritesPath)
            {
                spriteExtension.Add(Path.GetFileNameWithoutExtension(s), Path.GetExtension(s));
            }

            // fill tile set
            tileSet[Sprite.Player] = (Bitmap)Image.FromFile(path + "Player" + spriteExtension["Player"]);

            tileSet[Sprite.Start] = (Bitmap)Image.FromFile(path + "Start" + spriteExtension["Start"]);
            tileSet[Sprite.Finish] = (Bitmap)Image.FromFile(path + "Finish" + spriteExtension["Finish"]);

            tileSet[Sprite.Wall] = (Bitmap)Image.FromFile(path + "Wall" + spriteExtension["Wall"]);
            tileSet[Sprite.Floor] = (Bitmap)Image.FromFile(path + "Floor" + spriteExtension["Floor"]);
            tileSet[Sprite.Catcher] = (Bitmap)Image.FromFile(path + "Catcher" + spriteExtension["Catcher"]);
        }

        public static Bitmap GetSprite(Sprite sprite)
        {
            return tileSet[sprite];
        }
    }
}
