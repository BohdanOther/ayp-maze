﻿using System;
using System.Collections.Generic;
using System.IO;

namespace GameService.Common
{
    static class LevelLoader
    {
        public static int[][] LoadLevel(String levelName)
        {
            List<List<int>> jaggedList;
            using (StreamReader strread = new StreamReader(GameContext.ResourcesPath + @"Levels\" + levelName + @".txt"))
            {
                jaggedList = new List<List<int>>();

                String row;
                int i = 0;

                while ((row = strread.ReadLine()) != null)
                {
                    row = row.Trim();
                    String[] tokens = row.Split();
                    jaggedList.Add(new List<int>());
                    for (int j = 0; j < tokens.Length; j++)
                        jaggedList[i].Add(Int32.Parse(tokens[j]));
                    i++;
                }
            }

            int[][] map = new int[jaggedList.Count][];
            for (int i = 0; i < jaggedList.Count; i++)
            {
                map[i] = new int[jaggedList[i].Count];
                for (int j = 0; j < jaggedList[i].Count; j++)
                {
                    map[i][j] = jaggedList[i][j];
                }
            }

            return map;
        }
    }
}
