﻿namespace GameService.Common
{
    public class InputListener
    {
        public bool LMBPressed { get; set; }
        public PointFMaze MousePoint { get; set; }

        public InputListener()
        {
            this.MousePoint = new PointFMaze();
        }
    }
}
