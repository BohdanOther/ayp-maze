﻿using System;

namespace GameService.Common
{
    public class PointFMaze
    {
        public float X { set; get; }
        public float Y { set; get; }

        public PointFMaze()
        {
            this.X = 0;
            this.Y = 0;
        }
        public PointFMaze(float x, float y)
        {
            this.X = x;
            this.Y = y;
        }

        public PointFMaze Offset(float dx, float dy)
        {
            return new PointFMaze(this.X + dx, this.Y + dy);
        }

        public void Translate(float dx ,float dy)
        {
            this.X += dx;
            this.Y += dy;
        }
     
        public float Distance(PointFMaze b)
        {
            return (float)Math.Sqrt(Math.Pow(this.X - b.X, 2) + Math.Pow(this.X - b.Y, 2));
        }

        public static float Distance(PointFMaze a, PointFMaze b)
        {
            return (float)Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.X - b.Y, 2));
        }

        public PointFMaze Clone()
        {
            return new PointFMaze(this.X, this.Y);
        }
    }
}
