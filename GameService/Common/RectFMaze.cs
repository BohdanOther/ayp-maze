﻿namespace GameService.Common
{
    public class RectFMaze : IRectangle
    {
        public float PosX { get; set; }
        public float PosY { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }

        public RectFMaze(float posX, float posY, float width, float height)
        {
            this.PosX = posX;
            this.PosY = posY;
            this.Width = width;
            this.Height = height;
        }
    }
}
