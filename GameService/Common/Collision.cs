﻿namespace GameService.Common
{
    static class Collision
    {
        public static bool IsRectInsideAnother(IRectangle SmallRect, IRectangle LargeRect)
        {
            return (SmallRect.PosX >= LargeRect.PosX &&
                    SmallRect.PosX + SmallRect.Width <= LargeRect.PosX + LargeRect.Width &&
                    SmallRect.PosY > LargeRect.PosY &&
                    SmallRect.PosY + SmallRect.Height <= LargeRect.PosY + LargeRect.Height);
        }

        public static bool IsTwoRectanglesIntersect(IRectangle first, IRectangle second)
        {
            return
                 (
                     ((IsXWithinRectangle(second, first.PosX) || IsXWithinRectangle(second, first.PosX + first.Width)) &&
                     (IsYWithinRectangle(second, first.PosY) || IsYWithinRectangle(second, first.PosY + first.Height)))
                     ||
                     ((IsXWithinRectangle(first, second.PosX) || IsXWithinRectangle(first, second.PosX + second.Width)) &&
                     (IsYWithinRectangle(first, second.PosY) || IsYWithinRectangle(first, second.PosY + second.Height)))
                 );
        }

        public static bool IsXWithinRectangle(IRectangle rect, float x)
        {
            return x >= rect.PosX && x <= rect.PosX + rect.Width;
        }

        public static bool IsYWithinRectangle(IRectangle rect, float y)
        {
            return y >= rect.PosY && y <= rect.PosY + rect.Height;
        }
    }
}
