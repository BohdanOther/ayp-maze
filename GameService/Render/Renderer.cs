﻿using GameService.Entities;
using System.Collections.Generic;
using System.Drawing;

namespace GameService.Render
{
    internal sealed class Renderer : System.IDisposable
    {
        public List<IDrawable> DrawableContainer { get; private set; }

        public static Size ScreenDimension { get; private set; }

        public int Fps { get; set; }

        private Graphics gBuff;
        private Graphics gOffscreen;
        private Bitmap buffer;

        public Renderer(Graphics offscreen, Size resolution)
        {
            if (offscreen != null)
                this.gOffscreen = offscreen;

            buffer = new Bitmap(resolution.Width, resolution.Height,
                System.Drawing.Imaging.PixelFormat.Format32bppPArgb);

            gBuff = Graphics.FromImage(buffer);
            gBuff.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            ScreenDimension = resolution;

            DrawableContainer = new List<IDrawable>();
        }

        public void RenderFrame(Camera cam)
        {
            gBuff.Clear(Color.LightGray);

            foreach (IDrawable drawable in DrawableContainer)
            {
                drawable.Render(gBuff, cam);
            }

            if (GameContext.ShowFps)
            {
                gBuff.DrawString(Fps.ToString(), SystemFonts.StatusFont, Brushes.Coral, 10, 10);
            }

            try
            {
                gOffscreen.DrawImageUnscaled(buffer, 0, 0);
            }
            catch { }
        }

        public void Dispose()
        {
            buffer.Dispose();
        }
    }
}
