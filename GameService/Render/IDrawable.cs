﻿using GameService.Entities;
using System.Drawing;

namespace GameService.Render
{
    interface IDrawable
    {
        void Render(Graphics g, Camera camera);
    }
}
