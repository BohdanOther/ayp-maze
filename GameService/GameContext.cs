﻿using GameService.Common;
using System.IO;

namespace GameService
{
    public static class GameContext
    {
        private static RemoteContext remoteContext = new RemoteContext();
        public static RemoteContext RemoteContext { get { return remoteContext; } }
        //
        // GAMEPLAY Settings
        //
        public static GameParameters GameParameters { get; set; }
        
        public static readonly byte ShadowTransperency = 255;
        public static readonly int TileSize = 32;
        public static readonly int PlayerSize = 24;
        public static readonly float PlayerSpeed = 2.0f;

        public static string UserTheme { get; set; }
        //
        // Game settings
        //
        public static readonly string ResourcesPath = Path
            .GetDirectoryName(Path
            .GetDirectoryName(Directory
            .GetCurrentDirectory())) + @"\Resources\";

        public const int FpsRateLockAmount = 60;

        public static bool ShowFps { get; set; }

        public static readonly int CollisionDetectRadius = 1;
        public static readonly int TileRenderVisibilityRadius = 14;
        public static readonly int TileFovVisualRadius = 8;
    }
}
