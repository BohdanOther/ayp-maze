﻿using System;
namespace GameService
{
    public enum Difficulty
    {
        Easy,
        Normal,
        Hard
    }

    public enum GameMode
    {
        Classic,
        Arcade
    }

    [Serializable]
    public class GameParameters
    {
        public Difficulty Difficulty { get; set; }
        public GameMode GameMode { get; set; }

        public string LevelName { get; set; }

        public uint MazeSize { get; set; }
        public int[][] GeneratedLevel { get; set; }
    }
}
