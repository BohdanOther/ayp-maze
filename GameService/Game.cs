﻿using GameService.Common;
using GameService.Entities;
using GameService.Render;
using System;
using System.Drawing;
using System.Threading;

namespace GameService
{
    public sealed class Game : IDisposable
    {
        /// <summary>
        /// Turn on/off debug mode
        /// Shows rendering logic & FPS
        /// </summary>
        public static bool DebugMode { get; set; }

        // Game state flag
        public bool isRunning;

        // Drawing Objects
        private Renderer renderer;

        // Visual Effects Objects
        private Camera camera;
        private ShadowCaster shadowCaster;
        private FOV fov;

        // Player Movement Handler
        public InputListener inputListener;

        // Game Entities
        public World world;
        private Player localPlayer;
        private Player remotePlayer;

        private static NotificationManager notificationManger;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="offscreen"> graphic from component, ON WHICH we're drawing</param>
        /// <param name="resolution">offscreen resolution. buffer will get same resolution</param>
        public Game(Graphics offscreen, Size resolution, GameParameters parameters)
        {
            GameContext.GameParameters = parameters;
            SpriteLoader.LoadSprites(GameContext.UserTheme);

            renderer = new Renderer(offscreen, resolution);
            shadowCaster = new ShadowCaster();
            fov = new FOV();
            inputListener = new InputListener();
            camera = new Camera();
            notificationManger = new NotificationManager();
            world = new World();

        }

        public static void ShowNotification(string notification, int timeout)
        {
            notificationManger.Notify(notification, timeout);
        }

        public void ShowGlobalNotification(string message)
        {
            notificationManger.GlobalNotification(message, true);
        }

        public void HideGlobalNotification()
        {
            notificationManger.GlobalNotification("", false);
        }

        public void Start()
        {
            InitializeEntites();

            isRunning = true;

            Thread gameThread = new Thread(Run);
            gameThread.Start();
        }

        public void Stop()
        {
            isRunning = false;
        }

        private void Run()
        {
            int fps = 0;
            int fpsTimer = Environment.TickCount;

            const int SKIP_TICKS = 1000 / GameContext.FpsRateLockAmount;
            const int MAX_FRAMESKIP = 5;

            int loops;
            int next_game_tick = Environment.TickCount;

            while (isRunning)
            {
                //
                // UPDATE GAME STATE
                //
                loops = 0;
                while ((Environment.TickCount > next_game_tick) && (loops < MAX_FRAMESKIP))
                {
                    HandleLocalInput();

                    localPlayer.Update();

                    HandleRemoteInput();

                    CheckGameState();

                    notificationManger.Update();

                    world.Update(camera.CamTileX, camera.CamTileY);

                    camera.Update(localPlayer.PosX + localPlayer.Width / 2.0f,
                        localPlayer.PosY + localPlayer.Height / 2.0f);

                    fov.Update(camera.CamTileX, camera.CamTileY);

                    shadowCaster.Update(localPlayer.PosX + localPlayer.Width / 2.0f,
                        localPlayer.PosY + localPlayer.Height / 2.0f, fov.Blockers);

                    next_game_tick += SKIP_TICKS;
                    loops++;
                }
                //
                // RENDER GAME
                //
                renderer.RenderFrame(camera);
                //
                // COUNT FRAME RATE
                //
                if (Environment.TickCount - fpsTimer >= 1000)
                {
                    renderer.Fps = fps;
                    fps = 0;
                    fpsTimer = Environment.TickCount;
                }
                else fps++;
            }
        }

        private void CheckGameState()
        {
            if (localPlayer.ReachedFinish)
            {
                notificationManger.Notify("You won", 300);

                localPlayer.PosX = World.StartPoint.X;
                localPlayer.PosY = World.StartPoint.Y;

                localPlayer.ReachedFinish = false;
            }

            if (remotePlayer.ReachedFinish)
            {
                notificationManger.Notify("You Lost", 300);

                remotePlayer.ReachedFinish = false;
            }
        }

        private void HandleLocalInput()
        {
            localPlayer.IsMoving = inputListener.LMBPressed;
            localPlayer.PointingDir = inputListener.MousePoint.Offset(camera.CamX, camera.CamY);
        }

        private void HandleRemoteInput()
        {
            GameContext.RemoteContext.LocalX = (int)localPlayer.PosX;
            GameContext.RemoteContext.LocalY = (int)localPlayer.PosY;
            GameContext.RemoteContext.LocalFinishReached = localPlayer.ReachedFinish;

            remotePlayer.PosX = GameContext.RemoteContext.RemoteX;
            remotePlayer.PosY = GameContext.RemoteContext.RemoteY;
            remotePlayer.ReachedFinish = GameContext.RemoteContext.RemoteFinishReached;
        }

        public void Dispose()
        {
            renderer.Dispose();
            shadowCaster.Dispose();
        }

        private void InitializeEntites()
        {
            localPlayer = new Player(World.StartPoint.X, World.StartPoint.Y);
            remotePlayer = new Player(World.StartPoint.X, World.StartPoint.Y);

            GameContext.RemoteContext.RemoteX = (int)remotePlayer.PosX;
            GameContext.RemoteContext.RemoteY = (int)remotePlayer.PosY;

            // order is important!
            // this in drawn in order of addition
            renderer.DrawableContainer.Add(remotePlayer);
            renderer.DrawableContainer.Add(shadowCaster);
            renderer.DrawableContainer.Add(world);
            renderer.DrawableContainer.Add(fov);
            renderer.DrawableContainer.Add(localPlayer);
            renderer.DrawableContainer.Add(notificationManger);
        }
    }
}
