﻿using GameService.Entities.Tile;
using GameService.Render;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace GameService.Entities
{
    /// <summary>
    /// Class calculates field of view (FOV) from specific point
    /// </summary>
    internal class FOV : IDrawable
    {
        public List<Point> VisibleTiles { get; private set; }
        public HashSet<Point> Blockers { get; private set; }

        private delegate void FovScanner(int pDepth, double pStartSlope, double pEndSlope);
        private FovScanner scanner;

        private int FromTileX;
        private int FromTileY;

        private int visrange2 = (int)Math.Pow(GameContext.TileFovVisualRadius, 2);

        public FOV()
        {
            Blockers = new HashSet<Point>();
            VisibleTiles = new List<Point>();

            //
            //    \ 1 | 2 /
            //   8 \  |  / 3
            //   -----+-----
            //   7 /  |  \ 4
            //    / 6 | 5 \
            scanner += Scan1;
            scanner += Scan2;
            scanner += Scan3;
            scanner += Scan4;
            scanner += Scan5;
            scanner += Scan6;
            scanner += Scan7;
            scanner += Scan8;
        }

        /// <summary>
        /// Target position in tile coord meaning
        /// </summary>
        public void Update(int tilex, int tiley)
        {
            // if position hasn't changed, no update required
            if (FromTileX == tilex && FromTileY == tiley) return;

            FromTileX = tilex;
            FromTileY = tiley;

            CalculateVisibleTiles();
        }

        public void Render(Graphics g, Camera camera)
        {
            // TESTING
            if (Game.DebugMode)
            {
                // uncomment if want to see visible tiles marked
                foreach (Point p in VisibleTiles)
                {
                    float renderX = World.tileMap[p.Y][p.X].PosX - camera.CamX;
                    float renderY = World.tileMap[p.Y][p.X].PosY - camera.CamY;

                    g.DrawRectangle(Pens.LightGreen, renderX, renderY, GameContext.TileSize, GameContext.TileSize);
                }

                // uncomment if want to see blocker tiles marked
                foreach (Point p in Blockers)
                {
                    float renderX = World.tileMap[p.Y][p.X].PosX - camera.CamX;
                    float renderY = World.tileMap[p.Y][p.X].PosY - camera.CamY;

                    g.DrawRectangle(Pens.DarkRed, renderX, renderY, GameContext.TileSize, GameContext.TileSize);
                }
            }
        }

        private void CalculateVisibleTiles()
        {
            //
            // CALCULATE FOV FROM GIVEN STANDING POSITION
            //
            VisibleTiles.Clear();
            VisibleTiles.Add(new Point(FromTileX, FromTileY));
            scanner(1, 1, 0);
            //
            // Update world visibility
            //
            if (GameContext.GameParameters.Difficulty == Difficulty.Hard)
            {
                World.ResetAllWorldVisibility(FromTileX, FromTileY);
            }
            else if (GameContext.GameParameters.Difficulty == Difficulty.Normal)
            {
                World.ResetAreaVisibility(FromTileX, FromTileY);
            }

            Blockers.Clear();
            foreach (Point p in VisibleTiles)
            {
                World.tileMap[p.Y][p.X].Visible = true;
                World.tileMap[p.Y][p.X].Seen = true;

                foreach (Point blockp in World.tileMap[p.Y][p.X].SideBlockers)
                {

                    World.tileMap[blockp.Y][blockp.X].Visible = true;
                    World.tileMap[blockp.Y][blockp.X].Seen = true;

                    Blockers.Add(blockp);
                }
            }
        }

        private void Scan1(int pDepth, double pStartSlope, double pEndSlope)
        {
            int x = 0;
            int y = 0;

            y = FromTileY - pDepth;
            if (y < 0) return;

            x = FromTileX - Convert.ToInt32((pStartSlope * Convert.ToDouble(pDepth)));
            if (x < 0) x = 0;

            while (GetSlope(x, y, FromTileX, FromTileY, false) >= pEndSlope)
            {
                if (GetVisDistance(x, y, FromTileX, FromTileY) <= visrange2)
                {
                    if (World.tileMap[y][x].TileType == TileType.Wall) //current cell blocked
                    {
                        if (x - 1 >= 0 && World.tileMap[y][x - 1].TileType != TileType.Wall) //prior cell within range AND open...
                            //...incremenet the depth, adjust the endslope and recurse
                            Scan1(pDepth + 1, pStartSlope, GetSlope(x - 0.5, y + 0.5, FromTileX, FromTileY, false));
                    }
                    else
                    {

                        if (x - 1 >= 0 && World.tileMap[y][x - 1].TileType == TileType.Wall) //prior cell within range AND open...
                            //..adjust the startslope
                            pStartSlope = GetSlope(x - 0.5, y - 0.5, FromTileX, FromTileY, false);

                        VisibleTiles.Add(new Point(x, y));
                    }
                }
                x++;
            }
            x--;

            if (x < 0)
                x = 0;
            else if (x >= World.WorldWidth)
                x = World.WorldWidth - 1;

            if (y < 0)
                y = 0;
            else if (y >= World.WorldHeight)
                y = World.WorldHeight - 1;

            if (pDepth < GameContext.TileFovVisualRadius & World.tileMap[y][x].TileType != TileType.Wall)
                Scan1(pDepth + 1, pStartSlope, pEndSlope);
        }

        private void Scan2(int pDepth, double pStartSlope, double pEndSlope)
        {
            int x = 0;
            int y = 0;

            y = FromTileY - pDepth;
            if (y < 0) return;

            x = FromTileX + Convert.ToInt32((pStartSlope * Convert.ToDouble(pDepth)));
            if (x >= World.WorldWidth) x = World.WorldWidth - 1;

            while (GetSlope(x, y, FromTileX, FromTileY, false) <= pEndSlope)
            {
                if (GetVisDistance(x, y, FromTileX, FromTileY) <= visrange2)
                {
                    if (World.tileMap[y][x].TileType == TileType.Wall)
                    {
                        if (x + 1 < World.WorldWidth && World.tileMap[y][x + 1].TileType != TileType.Wall)
                            Scan2(pDepth + 1, pStartSlope, GetSlope(x + 0.5, y + 0.5, FromTileX, FromTileY, false));
                    }
                    else
                    {
                        if (x + 1 < World.WorldWidth && World.tileMap[y][x + 1].TileType == TileType.Wall)
                            pStartSlope = -GetSlope(x + 0.5, y - 0.5, FromTileX, FromTileY, false);

                        VisibleTiles.Add(new Point(x, y));
                    }
                }
                x--;
            }
            x++;

            if (x < 0)
                x = 0;
            else if (x >= World.WorldWidth)
                x = World.WorldWidth - 1;

            if (y < 0)
                y = 0;
            else if (y >= World.WorldHeight)
                y = World.WorldHeight - 1;

            if (pDepth < GameContext.TileFovVisualRadius & World.tileMap[y][x].TileType != TileType.Wall)
                Scan2(pDepth + 1, pStartSlope, pEndSlope);
        }

        private void Scan3(int pDepth, double pStartSlope, double pEndSlope)
        {
            int x = 0;
            int y = 0;

            x = FromTileX + pDepth;
            if (x >= World.WorldWidth) return;

            y = FromTileY - Convert.ToInt32((pStartSlope * Convert.ToDouble(pDepth)));
            if (y < 0) y = 0;

            while (GetSlope(x, y, FromTileX, FromTileY, true) <= pEndSlope)
            {
                if (GetVisDistance(x, y, FromTileX, FromTileY) <= visrange2)
                {
                    if (World.tileMap[y][x].TileType == TileType.Wall)
                    {
                        if (y - 1 >= 0 && World.tileMap[y - 1][x].TileType != TileType.Wall)
                            Scan3(pDepth + 1, pStartSlope, GetSlope(x - 0.5, y - 0.5, FromTileX, FromTileY, true));
                    }
                    else
                    {
                        if (y - 1 >= 0 && World.tileMap[y - 1][x].TileType == TileType.Wall)
                            pStartSlope = -GetSlope(x + 0.5, y - 0.5, FromTileX, FromTileY, true);

                        VisibleTiles.Add(new Point(x, y));
                    }
                }
                y++;
            }
            y--;

            if (x < 0)
                x = 0;
            else if (x >= World.WorldWidth)
                x = World.WorldWidth - 1;

            if (y < 0)
                y = 0;
            else if (y >= World.WorldHeight)
                y = World.WorldHeight - 1;

            if (pDepth < GameContext.TileFovVisualRadius & World.tileMap[y][x].TileType != TileType.Wall)
                Scan3(pDepth + 1, pStartSlope, pEndSlope);
        }

        private void Scan4(int pDepth, double pStartSlope, double pEndSlope)
        {
            int x = 0;
            int y = 0;

            x = FromTileX + pDepth;
            if (x >= World.WorldWidth) return;

            y = FromTileY + Convert.ToInt32((pStartSlope * Convert.ToDouble(pDepth)));
            if (y >= World.WorldHeight) y = World.WorldHeight - 1;

            while (GetSlope(x, y, FromTileX, FromTileY, true) >= pEndSlope)
            {
                if (GetVisDistance(x, y, FromTileX, FromTileY) <= visrange2)
                {
                    if (World.tileMap[y][x].TileType == TileType.Wall)
                    {
                        if (y + 1 < World.WorldHeight && World.tileMap[y + 1][x].TileType != TileType.Wall)
                            Scan4(pDepth + 1, pStartSlope, GetSlope(x - 0.5, y + 0.5, FromTileX, FromTileY, true));
                    }
                    else
                    {
                        if (y + 1 < World.WorldHeight && World.tileMap[y + 1][x].TileType == TileType.Wall)
                            pStartSlope = GetSlope(x + 0.5, y + 0.5, FromTileX, FromTileY, true);

                        VisibleTiles.Add(new Point(x, y));
                    }
                }
                y--;
            }
            y++;

            if (x < 0)
                x = 0;
            else if (x >= World.WorldWidth)
                x = World.WorldWidth - 1;

            if (y < 0)
                y = 0;
            else if (y >= World.WorldHeight)
                y = World.WorldHeight - 1;

            if (pDepth < GameContext.TileFovVisualRadius & World.tileMap[y][x].TileType != TileType.Wall)
                Scan4(pDepth + 1, pStartSlope, pEndSlope);
        }

        private void Scan5(int pDepth, double pStartSlope, double pEndSlope)
        {
            int x = 0;
            int y = 0;

            y = FromTileY + pDepth;
            if (y >= World.WorldHeight) return;

            x = FromTileX + Convert.ToInt32((pStartSlope * Convert.ToDouble(pDepth)));
            if (x >= World.WorldWidth) x = World.WorldWidth - 1;

            while (GetSlope(x, y, FromTileX, FromTileY, false) >= pEndSlope)
            {
                if (GetVisDistance(x, y, FromTileX, FromTileY) <= visrange2)
                {

                    if (World.tileMap[y][x].TileType == TileType.Wall)
                    {
                        if (x + 1 < World.WorldWidth && World.tileMap[y][x + 1].TileType != TileType.Wall)
                            Scan5(pDepth + 1, pStartSlope, GetSlope(x + 0.5, y - 0.5, FromTileX, FromTileY, false));
                    }
                    else
                    {
                        if (x + 1 < World.WorldWidth && World.tileMap[y][x + 1].TileType == TileType.Wall)
                            pStartSlope = GetSlope(x + 0.5, y + 0.5, FromTileX, FromTileY, false);

                        VisibleTiles.Add(new Point(x, y));
                    }
                }
                x--;
            }
            x++;

            if (x < 0)
                x = 0;
            else if (x >= World.WorldWidth)
                x = World.WorldWidth - 1;

            if (y < 0)
                y = 0;
            else if (y >= World.WorldHeight)
                y = World.WorldHeight - 1;

            if (pDepth < GameContext.TileFovVisualRadius & World.tileMap[y][x].TileType != TileType.Wall)
                Scan5(pDepth + 1, pStartSlope, pEndSlope);
        }

        private void Scan6(int pDepth, double pStartSlope, double pEndSlope)
        {
            int x = 0;
            int y = 0;

            y = FromTileY + pDepth;
            if (y >= World.WorldHeight) return;

            x = FromTileX - Convert.ToInt32((pStartSlope * Convert.ToDouble(pDepth)));
            if (x < 0) x = 0;

            while (GetSlope(x, y, FromTileX, FromTileY, false) <= pEndSlope)
            {
                if (GetVisDistance(x, y, FromTileX, FromTileY) <= visrange2)
                {
                    if (World.tileMap[y][x].TileType == TileType.Wall)
                    {
                        if (x - 1 >= 0 && World.tileMap[y][x - 1].TileType != TileType.Wall)
                            Scan6(pDepth + 1, pStartSlope, GetSlope(x - 0.5, y - 0.5, FromTileX, FromTileY, false));
                    }
                    else
                    {
                        if (x - 1 >= 0 && World.tileMap[y][x - 1].TileType == TileType.Wall)
                            pStartSlope = -GetSlope(x - 0.5, y + 0.5, FromTileX, FromTileY, false);

                        VisibleTiles.Add(new Point(x, y));
                    }
                }
                x++;
            }
            x--;

            if (x < 0)
                x = 0;
            else if (x >= World.WorldWidth)
                x = World.WorldWidth - 1;

            if (y < 0)
                y = 0;
            else if (y >= World.WorldHeight)
                y = World.WorldHeight - 1;

            if (pDepth < GameContext.TileFovVisualRadius & World.tileMap[y][x].TileType != TileType.Wall)
                Scan6(pDepth + 1, pStartSlope, pEndSlope);
        }

        private void Scan7(int pDepth, double pStartSlope, double pEndSlope)
        {
            int x = 0;
            int y = 0;

            x = FromTileX - pDepth;
            if (x < 0) return;

            y = FromTileY + Convert.ToInt32((pStartSlope * Convert.ToDouble(pDepth)));
            if (y >= World.WorldHeight) y = World.WorldHeight - 1;

            while (GetSlope(x, y, FromTileX, FromTileY, true) <= pEndSlope)
            {
                if (GetVisDistance(x, y, FromTileX, FromTileY) <= visrange2)
                {
                    if (World.tileMap[y][x].TileType == TileType.Wall)
                    {
                        if (y + 1 < World.WorldHeight && World.tileMap[y + 1][x].TileType != TileType.Wall)
                            Scan7(pDepth + 1, pStartSlope, GetSlope(x + 0.5, y + 0.5, FromTileX, FromTileY, true));
                    }
                    else
                    {
                        if (y + 1 < World.WorldHeight && World.tileMap[y + 1][x].TileType == TileType.Wall)
                            pStartSlope = -GetSlope(x - 0.5, y + 0.5, FromTileX, FromTileY, true);

                        VisibleTiles.Add(new Point(x, y));
                    }
                }
                y--;
            }
            y++;

            if (x < 0)
                x = 0;
            else if (x >= World.WorldWidth)
                x = World.WorldWidth - 1;

            if (y < 0)
                y = 0;
            else if (y >= World.WorldHeight)
                y = World.WorldHeight - 1;

            if (pDepth < GameContext.TileFovVisualRadius & World.tileMap[y][x].TileType != TileType.Wall)
                Scan7(pDepth + 1, pStartSlope, pEndSlope);
        }

        private void Scan8(int pDepth, double pStartSlope, double pEndSlope)
        {
            int x = 0;
            int y = 0;

            x = FromTileX - pDepth;
            if (x < 0) return;

            y = FromTileY - Convert.ToInt32((pStartSlope * Convert.ToDouble(pDepth)));
            if (y < 0) y = 0;

            while (GetSlope(x, y, FromTileX, FromTileY, true) >= pEndSlope)
            {
                if (GetVisDistance(x, y, FromTileX, FromTileY) <= visrange2)
                {
                    if (World.tileMap[y][x].TileType == TileType.Wall)
                    {
                        if (y - 1 >= 0 && World.tileMap[y - 1][x].TileType != TileType.Wall)
                            Scan8(pDepth + 1, pStartSlope, GetSlope(x + 0.5, y - 0.5, FromTileX, FromTileY, true));
                    }
                    else
                    {
                        if (y - 1 >= 0 && World.tileMap[y - 1][x].TileType == TileType.Wall)
                            pStartSlope = GetSlope(x - 0.5, y - 0.5, FromTileX, FromTileY, true);

                        VisibleTiles.Add(new Point(x, y));
                    }
                }
                y++;
            }
            y--;

            if (x < 0)
                x = 0;
            else if (x >= World.WorldWidth)
                x = World.WorldWidth - 1;

            if (y < 0)
                y = 0;
            else if (y >= World.WorldHeight)
                y = World.WorldHeight - 1;

            if (pDepth < GameContext.TileFovVisualRadius & World.tileMap[y][x].TileType != TileType.Wall)
                Scan8(pDepth + 1, pStartSlope, pEndSlope);
        }

        private double GetSlope(double pX1, double pY1, double pX2, double pY2, bool pInvert)
        {
            if (pInvert)
                return (pY1 - pY2) / (pX1 - pX2);
            else
                return (pX1 - pX2) / (pY1 - pY2);
        }

        private int GetVisDistance(int pX1, int pY1, int pX2, int pY2)
        {
            return ((pX1 - pX2) * (pX1 - pX2)) + ((pY1 - pY2) * (pY1 - pY2));
        }
    }
}
