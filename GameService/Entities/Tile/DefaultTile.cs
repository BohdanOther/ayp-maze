﻿using GameService.Common;
using GameService.Render;
using System.Collections.Generic;
using System.Drawing;

namespace GameService.Entities.Tile
{
    enum TileType
    {
        Floor = 0,
        Wall = 1,

        Start = 2,
        Finish = 3,

        Catcher = 5
    }

    /// <summary>
    /// Tile - is block, rectangle - our world is array ow tiles
    /// </summary>
    class DefaultTile : IRectangle, IDrawable
    {
        public TileType TileType { get; set; }
        public Sprite Sprite { get; set; }

        // collidable rectangle
        public float PosX { get; set; }
        public float PosY { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }

        // drawable rectangle (or any shape you like)
        public float RenderX { get; set; }
        public float RenderY { get; set; }
        public float RenderWidth { get; set; }
        public float RenderHeight { get; set; }

        public virtual bool Visible { get; set; }
        public bool Seen { get; set; }
        protected int MinVisSize { get; set; }
        protected int MaxVisSize { get; set; }

        public List<Point> SideBlockers { get; private set; }

        public DefaultTile(float x, float y, float width, float height, TileType tileType)
        {
            this.PosX = x;
            this.PosY = y;
            this.Width = width;
            this.Height = height;
            this.TileType = tileType;

            this.SideBlockers = new List<Point>(8);
            this.MinVisSize = 0;
            this.MaxVisSize = GameContext.TileSize;
        }

        public virtual void Update()
        {
            if (this.Visible)
            {
                this.RenderHeight += 1;
                this.RenderWidth += 1;

                if (this.RenderWidth > MaxVisSize) this.RenderWidth = MaxVisSize;
                if (this.RenderHeight > MaxVisSize) this.RenderHeight = MaxVisSize;
            }
            else
            {
                this.RenderWidth -= 1;
                this.RenderHeight -= 1;

                if (this.RenderWidth <= MinVisSize) this.RenderWidth = MinVisSize;
                if (this.RenderHeight <= MinVisSize) this.RenderHeight = MinVisSize;
            }
        }

        public void Render(Graphics g, Camera cam)
        {
            RenderX = (int)(PosX + (Width - RenderWidth) / 2 - cam.CamX);
            RenderY = (int)(PosY + (Height - RenderHeight) / 2 - cam.CamY);


            if (this.RenderWidth > 0 && this.RenderHeight > 0)
                g.DrawImage(SpriteLoader.GetSprite(Sprite), RenderX, RenderY, RenderWidth, RenderHeight);

        }
    }
}
