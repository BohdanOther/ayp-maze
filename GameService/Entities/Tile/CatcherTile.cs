﻿namespace GameService.Entities.Tile
{
    class CatcherTile : DefaultTile
    {
        private readonly int DefaultDelay = 90;

        public int Delay { get; set; }

        public CatcherTile(DefaultTile tile)
            : base(tile.PosX, tile.PosY, tile.Width, tile.Height, TileType.Catcher)
        {
            this.Delay = DefaultDelay;
            this.Sprite = Common.Sprite.Catcher;
            this.MinVisSize = 5;
        }

        public override bool Visible
        {
            get
            {
                return base.Visible;
            }
            set
            {
                if (this.Delay < 0)
                {
                    base.Visible = value;
                }
            }
        }

        public override void Update()
        {
            base.Update();

            Delay--;
            if (Delay < 0)
            {
                this.Visible = !(bool)this.Visible;
                Delay = DefaultDelay;
            }
        }
    }
}
