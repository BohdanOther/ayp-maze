﻿using GameService.Common;
using GameService.Render;
using System;

namespace GameService.Entities
{
    public class Camera
    {
        public float CamX { get; set; }
        public float CamY { get; set; }

        public int CamTileX { get; private set; }
        public int CamTileY { get; private set; }

        private float targetX;
        private float targetY;
        private float cameraFollowSpeed = 1.5f;

        private readonly float hsx = Renderer.ScreenDimension.Width / 2.0f;
        private readonly float hsy = Renderer.ScreenDimension.Height / 2.0f;

        public void Update(float x, float y)
        {
            targetX = x - hsx;
            targetY = y - hsy;

            #region Follow script

            if (Math.Abs(targetX - CamX) > 600)
            {
                CamX = targetX + 150;
            }

            if (Math.Abs(targetY - CamY) > 600)
            {
                CamY = targetY + 150;
            }

            if (Math.Abs(targetX - CamX) > 40)
            {
                if (CamX < targetX)
                {
                    CamX += cameraFollowSpeed;
                }
                else if (CamX > targetX)
                {
                    CamX -= cameraFollowSpeed;
                }
            }

            if (Math.Abs(targetY - CamY) > 40)
            {
                if (CamY < targetY)
                {
                    CamY += cameraFollowSpeed;
                }
                else if (CamY > targetY)
                {
                    CamY -= cameraFollowSpeed;
                }
            } 
            #endregion

            float posX = targetX + hsx;
            float posY = targetY + hsy;

            CamTileX = (int)posX / (int)GameContext.TileSize;
            CamTileY = (int)posY / (int)GameContext.TileSize;
        }
    }
}
