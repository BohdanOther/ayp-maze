﻿using GameService.Entities.Tile;
using GameService.Render;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace GameService.Entities
{
    struct Ray
    {
        /// <summary>
        /// Angle in radians!
        /// </summary>
        public float angle;
        public PointF point;

        public Ray(float a, PointF p)
        {
            angle = a;
            point = p;
        }
    }

    sealed class ShadowCaster : IDrawable, IDisposable
    {
        private float fromX;
        private float fromY;

        private List<PointF> extremePoints;
        private List<Ray> rayList;

        private SolidBrush testRayColorBrush;
        private Pen testRayPen;

        public ShadowCaster()
        {
            extremePoints = new List<PointF>();
            rayList = new List<Ray>();

            testRayColorBrush = new SolidBrush(Color.FromArgb(GameContext.ShadowTransperency, 40, 40, 40));
            testRayPen = new Pen(Color.FromArgb(200, 250, 100, 100));
        }

        /// <summary>
        /// Update method
        /// </summary>
        /// <param name="fromX">cast X position</param>
        /// <param name="fromY">cast Y position</param>
        /// <param name="blocker">List of tiles which block fov</param>
        public void Update(float fromX, float fromY, HashSet<Point> blocker)
        {
            if (fromX != this.fromX || fromY != this.fromY)
            {
                this.fromX = fromX;
                this.fromY = fromY;

                CastRays(blocker);
            }
        }

        public void Render(Graphics g, Camera camera)
        {
            PointF[] trapez = new PointF[4];
            for (int i = 0; i < rayList.Count - 1; i += 2)
            {
                float length = Renderer.ScreenDimension.Width;
                //
                //
                //
                float dx1 = (float)Math.Cos(rayList[i].angle);
                float dy1 = (float)Math.Sin(rayList[i].angle);

                float x1 = rayList[i].point.X + dx1 * length;
                float y1 = rayList[i].point.Y + dy1 * length;
                //
                //
                //
                float dx2 = (float)Math.Cos(rayList[i + 1].angle);
                float dy2 = (float)Math.Sin(rayList[i + 1].angle);

                float x2 = rayList[i + 1].point.X + dx2 * length;
                float y2 = rayList[i + 1].point.Y + dy2 * length;

                trapez[0] = new PointF(rayList[i].point.X - camera.CamX, rayList[i].point.Y - camera.CamY);
                trapez[1] = new PointF(x1 - camera.CamX, y1 - camera.CamY);
                trapez[2] = new PointF(x2 - camera.CamX, y2 - camera.CamY);
                trapez[3] = new PointF(rayList[i + 1].point.X - camera.CamX, rayList[i + 1].point.Y - camera.CamY);

                g.FillPolygon(testRayColorBrush, trapez);
            }
            //
            // TESTING
            //
            if (Game.DebugMode)
            {
                for (int i = 0; i < extremePoints.Count; i += 2)
                {
                    g.DrawLine(testRayPen, fromX - camera.CamX, fromY - camera.CamY, extremePoints[i].X - camera.CamX, extremePoints[i].Y - camera.CamY);
                    g.DrawLine(testRayPen, fromX - camera.CamX, fromY - camera.CamY, extremePoints[i + 1].X - camera.CamX, extremePoints[i + 1].Y - camera.CamY);
                    g.DrawLine(testRayPen, extremePoints[i].X - camera.CamX, extremePoints[i].Y - camera.CamY, extremePoints[i + 1].X - camera.CamX, extremePoints[i + 1].Y - camera.CamY);
                }
            }
        }

        private void CastRays(HashSet<Point> blocker)
        {
            rayList.Clear();
            extremePoints.Clear();
            //
            // CALCULATE EXTREME POINTS
            //
            #region Extreme points clculation

            foreach (Point p in blocker)
            {
                DefaultTile t = World.tileMap[p.Y][ p.X];

                int tileX = (int)t.PosX / (int)GameContext.TileSize;
                int tileY = (int)t.PosY / (int)GameContext.TileSize;

                if (t.TileType != TileType.Wall || tileX == 0 || tileX == World.WorldWidth - 1 || tileY == 0 || tileY == World.WorldHeight - 1)
                {
                    continue;
                }
                else
                {
                    if (fromX > t.PosX && fromX < t.PosX + t.Width)
                    {
                        // top
                        if (fromY < t.PosY)
                        {
                            extremePoints.Add(new PointF(t.PosX, t.PosY + 1));
                            extremePoints.Add(new PointF(t.PosX + t.RenderWidth, t.PosY + 1));
                        }
                        //bottom
                        else if (fromY > t.PosY + t.Height)
                        {
                            extremePoints.Add(new PointF(t.PosX, t.PosY + t.RenderHeight - 1));
                            extremePoints.Add(new PointF(t.PosX + t.RenderWidth, t.PosY + t.RenderHeight - 1));
                        }
                    }
                    else if (fromY > t.PosY && fromY < t.PosY + t.Height)
                    {
                        //left
                        if (fromX < t.PosX)
                        {
                            extremePoints.Add(new PointF(t.PosX + 1, t.PosY));
                            extremePoints.Add(new PointF(t.PosX + 1, t.PosY + t.RenderHeight));
                        }
                        // right
                        else if (fromX > t.PosX + t.Width)
                        {
                            extremePoints.Add(new PointF(t.PosX + t.RenderWidth - 1, t.PosY));
                            extremePoints.Add(new PointF(t.PosX + t.RenderWidth - 1, t.PosY + t.RenderHeight));
                        }
                    }
                    // TOP CORNERS
                    if (fromY < t.PosY)
                    {
                        if (fromX < t.PosX)
                        {
                            extremePoints.Add(new PointF(t.PosX, t.PosY + t.RenderHeight));
                            extremePoints.Add(new PointF(t.PosX + t.RenderWidth, t.PosY));
                        }
                        else if (fromX > t.PosX + t.Width)
                        {
                            extremePoints.Add(new PointF(t.PosX, t.PosY));
                            extremePoints.Add(new PointF(t.PosX + t.RenderWidth, t.PosY + t.RenderHeight));
                        }
                    }
                    // BOTTOM CORNERS
                    else if (fromY > t.PosY + t.Height)
                    {
                        if (fromX < t.PosX)
                        {
                            extremePoints.Add(new PointF(t.PosX, t.PosY));
                            extremePoints.Add(new PointF(t.PosX + t.RenderWidth, t.PosY + t.RenderHeight));
                        }
                        else if (fromX > t.PosX + t.Width)
                        {
                            extremePoints.Add(new PointF(t.PosX, t.PosY + t.RenderHeight));
                            extremePoints.Add(new PointF(t.PosX + t.RenderWidth, t.PosY));
                        }
                    }
                }
            }
            #endregion
            //
            // CAST RAYS THROUGH THEM
            //
            foreach (PointF p in extremePoints)
            {
                float alpha = (float)(Math.Atan2(p.Y - fromY, p.X - fromX));

                rayList.Add(new Ray(alpha, p));
            }
        }

        public void Dispose()
        {
            testRayColorBrush.Dispose();
            testRayPen.Dispose();
        }
    }
}
