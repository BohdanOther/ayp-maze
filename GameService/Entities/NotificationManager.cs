﻿using GameService.Render;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;

namespace GameService.Entities
{
    class Notification
    {
        public string message;
        public int delay;

        public Notification(string m, int d)
        {
            message = m;
            delay = d;
        }

        public void Update()
        {
            delay--;
        }
    }

    struct GlobalNotification
    {
        public string Message;
        public string Anim;
        public bool Show;

        private int dots;
        private int doUpdate;

        public GlobalNotification(string m, bool b)
        {
            Message = m;
            Anim = "";
            Show = b;
            dots = 0;
            doUpdate = 30;
        }

        public void Update()
        {
            doUpdate--;
            if (doUpdate < 0)
            {
                dots++;
                if (dots > 4) dots = 0;
                Anim = new string('.', dots);
                doUpdate = 30;
            }
        }
    }

    internal sealed class NotificationManager : IDrawable, System.IDisposable
    {
        private List<Notification> MessageQueue;
        private GlobalNotification gNote;

        PrivateFontCollection pfc;
        Font notificationFont;
        Font gNoteFont;

        SolidBrush highlightRectColor;
        SolidBrush globalrectColor;
        Rectangle highlightRect;

        public NotificationManager()
        {
            MessageQueue = new List<Notification>();

            pfc = new PrivateFontCollection();
            pfc.AddFontFile(GameService.GameContext.ResourcesPath + @"Walkway_UltraBold.ttf");

            notificationFont = new Font(
            pfc.Families[0],
            14.25f,
            FontStyle.Bold,
            GraphicsUnit.Point);

            gNoteFont = new Font(
               pfc.Families[0],
            20.25f,
            FontStyle.Regular,
            GraphicsUnit.Point);

            highlightRectColor = new SolidBrush(Color.FromArgb(100, 200, 200, 200));
            globalrectColor = new SolidBrush(Color.FromArgb(160, 40, 40, 40));
            highlightRect = new Rectangle(10, 450, 150, 70);
        }

        public void Notify(string message, int delay)
        {
            MessageQueue.Add(new Notification(message, delay));
        }

        public void GlobalNotification(string message, bool visible)
        {
            gNote = new GlobalNotification(message, visible);
        }

        public void Update()
        {
            for (int i = 0; i < MessageQueue.Count; i++)
            {
                MessageQueue[i].Update();
                if (MessageQueue[i].delay <= 0) MessageQueue.RemoveAt(i);
            }

            gNote.Update();

            highlightRect.Height = MessageQueue.Count * 20 + 20;
            highlightRect.Y = 500 - MessageQueue.Count * 20;
        }

        public void Render(Graphics g, Camera camera)
        {
            if (MessageQueue.Count > 0) g.FillRectangle(highlightRectColor, highlightRect);

            for (int i = 0; i < MessageQueue.Count; i++)
            {
                g.DrawString(MessageQueue[i].message, notificationFont, Brushes.WhiteSmoke,
                    highlightRect.X + 10, highlightRect.Y + highlightRect.Height - 10 - (i + 1) * 20);
            }

            if (gNote.Show)
            {
                g.FillRectangle(globalrectColor, 0, Renderer.ScreenDimension.Height / 2 - 50, Renderer.ScreenDimension.Width, 100);
                g.DrawString(gNote.Message + gNote.Anim, gNoteFont, Brushes.White, 100, Renderer.ScreenDimension.Height / 2 - 10);
            }
        }

        public void Dispose()
        {
            pfc.Dispose();
            notificationFont.Dispose();
            highlightRectColor.Dispose();
        }
    }
}