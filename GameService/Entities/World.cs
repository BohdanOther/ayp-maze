﻿using GameService.Common;
using GameService.Entities.Tile;
using GameService.Render;
using System;
using System.Drawing;

namespace GameService.Entities
{
    public class World : IDrawable
    {
        public static int WorldWidth { get; private set; }
        public static int WorldHeight { get; private set; }

        public static PointFMaze StartPoint;
        public static PointFMaze FinishPoint;

        internal static DefaultTile[][] tileMap;

        /// <summary>
        /// Do as much as possible here
        /// because this is executed only once at the very beginning
        /// </summary>
        public void LoadLevel(String levelName)
        {
            int[][] rawmap = LevelLoader.LoadLevel(levelName);

            GenerateLevelFromRawMap(rawmap);
        }

        public void GenerateLevel()
        {
            AutoGeneration.Generator generator = new AutoGeneration.Generator();

            int[][] generated = generator.GetMaze(GameContext.GameParameters.MazeSize);

            GenerateLevelFromRawMap(generated);
            GameContext.GameParameters.GeneratedLevel = generated;
        }

        /// <summary>
        /// Converter from map of ones&zeros into tile map
        /// </summary>
        /// <param name="rawmap"></param>
        public void GenerateLevelFromRawMap(int[][] rawmap)
        {
            WorldWidth = rawmap[0].Length;
            WorldHeight = rawmap.Length;

            // jagged array initialization
            tileMap = new DefaultTile[WorldHeight][];
            for (int i = 0; i < WorldHeight; i++)
            {
                tileMap[i] = new DefaultTile[WorldWidth];
            }

            for (int i = 0; i < WorldHeight; i++)
            {
                for (int j = 0; j < WorldWidth; j++)
                {
                    tileMap[i][j] = new DefaultTile(j * GameContext.TileSize, i * GameContext.TileSize,
                        GameContext.TileSize, GameContext.TileSize, (TileType)rawmap[i][j]);

                    if (GameContext.GameParameters.Difficulty == Difficulty.Easy)
                    {
                        tileMap[i][j].Visible = true;
                    }

                    switch (tileMap[i][j].TileType)
                    {
                        case TileType.Wall:
                            tileMap[i][j].Sprite = Sprite.Wall;
                            break;

                        case TileType.Floor:
                            tileMap[i][j].Sprite = Sprite.Floor;
                            break;

                        case TileType.Start:
                            tileMap[i][j].Sprite = Sprite.Start;
                            StartPoint = new PointFMaze(j * GameContext.TileSize + 5, i * GameContext.TileSize + 5);
                            break;

                        case TileType.Finish:
                            tileMap[i][j].Sprite = Sprite.Finish;
                            FinishPoint = new PointFMaze(j * GameContext.TileSize, i * GameContext.TileSize);
                            break;

                        case TileType.Catcher:
                            tileMap[i][j] = new CatcherTile(tileMap[i][j]);
                            break;
                    }
                    //
                    // Calculate side blockers for tile
                    //
                    if (rawmap[i][j] == 1 || rawmap[i][j] == 5 || i == 0 || i == World.WorldHeight - 1 || j == 0 || j == World.WorldWidth - 1)
                    {
                        continue;
                    }
                    else
                    {
                        for (int k = -1; k <= 1; k++)
                        {
                            for (int l = -1; l <= 1; l++)
                            {
                                if (rawmap[i + k][j + l] == 1)
                                {
                                    tileMap[i][j].SideBlockers.Add(new Point(j + l, i + k));
                                }
                            }
                        }
                    }
                }
            }
        }

        public void Update(int fromX, int fromY)
        {
            for (int i = Math.Max(fromY - GameContext.TileRenderVisibilityRadius, 0); i < Math.Min(fromY + GameContext.TileRenderVisibilityRadius, WorldHeight); i++)
            {
                for (int j = Math.Max(fromX - GameContext.TileRenderVisibilityRadius, 0); j < Math.Min(fromX + GameContext.TileRenderVisibilityRadius, WorldWidth); j++)
                {
                    tileMap[i][j].Update();
                }
            }
        }

        public void Render(Graphics g, Camera cam)
        {
            for (int i = Math.Max(cam.CamTileY - GameContext.TileRenderVisibilityRadius, 0); i < Math.Min(cam.CamTileY + GameContext.TileRenderVisibilityRadius, WorldHeight); i++)
            {
                for (int j = Math.Max(cam.CamTileX - GameContext.TileRenderVisibilityRadius, 0); j < Math.Min(cam.CamTileX + GameContext.TileRenderVisibilityRadius, WorldWidth); j++)
                {
                    tileMap[i][j].Render(g, cam);
                }
            }
        }

        public static void ResetAreaVisibility(int FromTileX, int FromTileY)
        {
            int topI = Math.Max(FromTileY - GameContext.TileRenderVisibilityRadius, 0);
            int downI = Math.Min(FromTileY + GameContext.TileRenderVisibilityRadius, WorldHeight);

            int leftJ = Math.Max(FromTileX - GameContext.TileRenderVisibilityRadius, 0);
            int rightJ = Math.Min(FromTileX + GameContext.TileRenderVisibilityRadius, WorldWidth);

            for (int i = topI; i < downI; i++)
            {
                for (int j = leftJ; j < rightJ; j++)
                {
                    if (((int)Math.Abs(i - FromTileY) > 9) || ((int)Math.Abs(j - FromTileX) > 10))
                    {
                        tileMap[i][j].Visible = false;
                    }
                    else if (tileMap[i][j].Seen)
                    {
                        tileMap[i][j].Visible = true;
                    }
                }
            }
        }

        public static void ResetAllWorldVisibility(int FromTileX, int FromTileY)
        {
            int topI = Math.Max(FromTileY - GameContext.TileRenderVisibilityRadius, 0);
            int downI = Math.Min(FromTileY + GameContext.TileRenderVisibilityRadius, WorldHeight);

            int leftJ = Math.Max(FromTileX - GameContext.TileRenderVisibilityRadius, 0);
            int rightJ = Math.Min(FromTileX + GameContext.TileRenderVisibilityRadius, WorldWidth);

            for (int i = topI; i < downI; i++)
            {
                for (int j = leftJ; j < rightJ; j++)
                {
                    tileMap[i][j].Visible = false;
                }
            }

        }
    }
}
