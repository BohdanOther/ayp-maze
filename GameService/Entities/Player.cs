﻿using GameService.Common;
using GameService.Entities.Tile;
using GameService.Render;
using System;
using System.Drawing;

namespace GameService.Entities
{
    class Player : IDrawable, IRectangle
    {
        public float PosX { get; set; }
        public float PosY { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }

        public PointFMaze PointingDir { get; set; }

        public bool IsMoving { get; set; }

        public bool ReachedFinish { get; set; }

        private float dx;
        private float dy;
        private float speed;

        public Player(float spawnX, float spawnY)
        {
            this.PosX = spawnX;
            this.PosY = spawnY;

            this.Width = GameContext.PlayerSize;
            this.Height = GameContext.PlayerSize;
            this.speed = GameContext.PlayerSpeed;

            this.PointingDir = new PointFMaze();
        }

        public void Update()
        {
            float len = (float)
                Math.Sqrt(
                Math.Pow((PointingDir.X - PosX - Width / 2.0f), 2) +
                Math.Pow((PointingDir.Y - PosY - Height / 2.0f), 2)
                );

            if (len > 30)
            {
                dx = (PointingDir.X - PosX - Width / 2.0f) / len;
                dy = (PointingDir.Y - PosY - Height / 2.0f) / len;
            }

            if (IsMoving)
                Move(dx, dy);
        }

        public void Render(Graphics g, Camera cam)
        {
            float renderX = PosX + Width / 2.0f - cam.CamX;
            float renderY = PosY + Height / 2.0f - cam.CamY;

            // useless circle
            g.DrawEllipse(Pens.Gray, renderX - Width * 2, renderY - Height * 2, Width * 4, Height * 4);

            // useless pointing point
            g.FillEllipse(Brushes.DarkRed, renderX + dx * Width * 2 - 3, renderY + dy * Height * 2 - 3, 6, 6);

            // player rectangle
            g.DrawImage(SpriteLoader.GetSprite(Sprite.Player), renderX - Width / 2.0f, renderY - Height / 2.0f, Width, Height);
        }

        private void Move(float dx, float dy)
        {
            if (!IsColliding(PosX + dx * speed, PosY))
            {
                PosX += dx * speed;
            }
            if (!IsColliding(PosX, PosY + dy * speed))
            {
                PosY += dy * speed;
            }
        }

        private bool IsColliding(float nextX, float nextY)
        {
            RectFMaze playerOnHisFuturePosition = new RectFMaze(nextX, nextY, this.Width, this.Height);

            int tileX = (int)PosX / (int)GameContext.TileSize;
            int tileY = (int)PosY / (int)GameContext.TileSize;

            for (int i = Math.Max(tileY - GameContext.CollisionDetectRadius, 0); i <= Math.Min(tileY + GameContext.CollisionDetectRadius, World.WorldHeight); i++)
            {
                for (int j = Math.Max(tileX - GameContext.CollisionDetectRadius, 0); j <= Math.Min(tileX + GameContext.CollisionDetectRadius, World.WorldWidth); j++)
                {
                    bool colided = Collision.IsTwoRectanglesIntersect(World.tileMap[i][j], playerOnHisFuturePosition);

                    if (!colided) continue;

                    switch (World.tileMap[i][j].TileType)
                    {
                        case TileType.Catcher:
                            if (World.tileMap[i][j].Visible)
                            {
                                this.PosX = World.StartPoint.X;
                                this.PosY = World.StartPoint.Y;
                                Game.ShowNotification("Caught you  !", 200);
                            }
                            break;

                        case TileType.Finish:
                            ReachedFinish = true;
                            break;

                        case TileType.Wall:
                            return true;
                    }
                }
            }
            return false;
        }
    }
}

