﻿using System;

namespace AutoGeneration
{
    public class Generator
    {
        private int Width, Height;

        public int[][] GetMaze(uint MazeSize, Algorithm AlgorithmType = Algorithm.SideWinder)
        {
            int size = (int)(MazeSize % 2 == 0 ? MazeSize + 1 : MazeSize);

            Width = Height = size;

            switch (AlgorithmType)
            {
                case Algorithm.SideWinder:
                    return GetSideWinderMaze();
                default:
                    return null;
            }
        }

        private int[][] GetEllerMaze()
        {
            return null;
        }

        private int[][] GetSideWinderMaze()
        {
            //creating base of the matrix with first empty row
            int[][] mazeField = new int[Height][];
            for (int i = 0; i < Height; i++)
            {
                mazeField[i] = new int[Width];
            }

            for (int rowNumber = 1; rowNumber < Height; rowNumber++)
                for (int columnNumber = 0; columnNumber < Width; columnNumber++)
                    mazeField[rowNumber][columnNumber] = 1;

            Random randomNumberGenerator = new Random();

            for (int rowNumber = 2; rowNumber < Height; rowNumber += 2)
            {
                bool isCurrentRawCompleted = false;
                int currentColumnNumber = 0;
                while (!isCurrentRawCompleted)
                {
                    int cellsAmount = randomNumberGenerator.Next(1, 5);
                    int cellsRightSide = Math.Min(currentColumnNumber + cellsAmount * 2, Width - 1);

                    for (int columnNumber = currentColumnNumber; columnNumber <= cellsRightSide; columnNumber++)
                    {
                        mazeField[rowNumber][columnNumber] = 0;
                    }

                    int TopCarvePoint = Math.Min(currentColumnNumber + randomNumberGenerator.Next(1, cellsAmount) * 2, Width - 1);

                    mazeField[rowNumber - 1][TopCarvePoint] = 0;

                    currentColumnNumber = cellsRightSide + 2;

                    if (currentColumnNumber >= Height)
                        isCurrentRawCompleted = true;
                }
            }

            mazeField = AddStartAndFinish(mazeField);
            mazeField = AddBoundaries(mazeField);

            //PrintTheMatrix(mazeField);
            return mazeField;
        }

        private int[][] AddBoundaries(int[][] input)
        {
            int[][] resultMatrix = new int[Height + 2][];
            for (int i = 0; i < Height + 2; i++)
            {
                resultMatrix[i] = new int[Width + 2];
            }

            for (int rowNumber = 0; rowNumber < Height + 2; rowNumber++)
            {
                resultMatrix[rowNumber][0] = resultMatrix[rowNumber][Width + 1] = 1;
            }

            for (int columnNumber = 0; columnNumber < Width + 2; columnNumber++)
            {
                resultMatrix[0][columnNumber] = resultMatrix[Height + 1][columnNumber] = 1;
            }
            for (int rowNumber = 0; rowNumber < Height; rowNumber++)
                for (int columnNumber = 0; columnNumber < Width; columnNumber++)
                    resultMatrix[rowNumber + 1][columnNumber + 1] = input[rowNumber][columnNumber];
            Width += 2;
            Height += 2;
            return resultMatrix;
        }

        private int[][] AddStartAndFinish(int[][] input)
        {
            //
            //    0 | 3
            //    -----
            //    1 | 2
            //
            Random randomNumberGenerator = new Random();
            int startSquare = randomNumberGenerator.Next(0, 3);
            int finishSquare = (startSquare + 2) % 4;

            int StartXPosition, StartYPosition, FinishXPosition, FinishYPosition;

            bool needChangePosition = true;
            while (needChangePosition)
            {
                StartXPosition = randomNumberGenerator.Next(0, Width / 2);
                if (startSquare == 3 || startSquare == 2) StartXPosition += Width / 2;

                StartYPosition = randomNumberGenerator.Next(0, Width / 2);
                if (startSquare == 1 || startSquare == 2) StartYPosition += Width / 2;

                if (input[StartYPosition][StartXPosition] == 0)
                {
                    needChangePosition = false;
                    input[StartYPosition][StartXPosition] = 2;
                }
            }
            needChangePosition = true;
            while (needChangePosition)
            {
                FinishXPosition = randomNumberGenerator.Next(0, Width / 2);
                if (finishSquare == 3 || finishSquare == 2) FinishXPosition += Width / 2;

                FinishYPosition = randomNumberGenerator.Next(0, Width / 2);
                if (finishSquare == 1 || finishSquare == 2) FinishYPosition += Width / 2;

                if (input[FinishYPosition][FinishXPosition] == 0)
                {
                    needChangePosition = false;
                    input[FinishYPosition][FinishXPosition] = 3;
                }
            }
            return input;
        }

        private void PrintTheMatrix(int[][] matrix)
        {
            for (int i = 0; i < Height; i++)
            {
                Console.WriteLine();
                for (int j = 0; j < Width; j++)
                {
                    switch (matrix[i][j])
                    {
                        case 0:
                            Console.Write('.');
                            break;
                        case 1:
                            Console.Write('#');
                            break;
                        case 2:
                            Console.Write('S');
                            break;
                        case 3:
                            Console.Write('F');
                            break;
                    }
                }
            }

        }
    }
}
