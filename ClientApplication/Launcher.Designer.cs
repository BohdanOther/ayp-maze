﻿namespace ClientApplication
{
    partial class FormLauncher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
                pfc.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabContainer = new System.Windows.Forms.TabControl();
            this.tabpageMainMenu = new System.Windows.Forms.TabPage();
            this.btnSettings = new System.Windows.Forms.Button();
            this.btnStartGame = new System.Windows.Forms.Button();
            this.btnExitGame = new System.Windows.Forms.Button();
            this.tabpageStartGame = new System.Windows.Forms.TabPage();
            this.panelStartGameSubmenu = new System.Windows.Forms.Panel();
            this.cbMazeSize = new System.Windows.Forms.ComboBox();
            this.cbGameMode = new System.Windows.Forms.ComboBox();
            this.cbDifficulty = new System.Windows.Forms.ComboBox();
            this.cbLevel = new System.Windows.Forms.ComboBox();
            this.btnBegin = new System.Windows.Forms.Button();
            this.textBoxIpPort = new System.Windows.Forms.TextBox();
            this.labelPortIp = new System.Windows.Forms.Label();
            this.btnBackStartGame = new System.Windows.Forms.Button();
            this.btnHost = new System.Windows.Forms.Button();
            this.btnJoin = new System.Windows.Forms.Button();
            this.tabpageSettings = new System.Windows.Forms.TabPage();
            this.cbThemeSelect = new System.Windows.Forms.ComboBox();
            this.btnPlayMusic = new System.Windows.Forms.Button();
            this.pbMusicOnOff = new System.Windows.Forms.PictureBox();
            this.btnBackSettings = new System.Windows.Forms.Button();
            this.tabContainer.SuspendLayout();
            this.tabpageMainMenu.SuspendLayout();
            this.tabpageStartGame.SuspendLayout();
            this.panelStartGameSubmenu.SuspendLayout();
            this.tabpageSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbMusicOnOff)).BeginInit();
            this.SuspendLayout();
            // 
            // tabContainer
            // 
            this.tabContainer.Controls.Add(this.tabpageMainMenu);
            this.tabContainer.Controls.Add(this.tabpageStartGame);
            this.tabContainer.Controls.Add(this.tabpageSettings);
            this.tabContainer.Location = new System.Drawing.Point(-4, -22);
            this.tabContainer.Margin = new System.Windows.Forms.Padding(0);
            this.tabContainer.Name = "tabContainer";
            this.tabContainer.SelectedIndex = 0;
            this.tabContainer.Size = new System.Drawing.Size(613, 427);
            this.tabContainer.TabIndex = 24;
            this.tabContainer.MouseDown += new System.Windows.Forms.MouseEventHandler(this.APIInterrupt_MouseDown);
            // 
            // tabpageMainMenu
            // 
            this.tabpageMainMenu.BackColor = System.Drawing.Color.Transparent;
            this.tabpageMainMenu.BackgroundImage = global::ClientApplication.Properties.Resources.menu_screen;
            this.tabpageMainMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabpageMainMenu.Controls.Add(this.btnSettings);
            this.tabpageMainMenu.Controls.Add(this.btnStartGame);
            this.tabpageMainMenu.Controls.Add(this.btnExitGame);
            this.tabpageMainMenu.Location = new System.Drawing.Point(4, 22);
            this.tabpageMainMenu.Margin = new System.Windows.Forms.Padding(0);
            this.tabpageMainMenu.Name = "tabpageMainMenu";
            this.tabpageMainMenu.Size = new System.Drawing.Size(605, 401);
            this.tabpageMainMenu.TabIndex = 0;
            this.tabpageMainMenu.Text = "MainMenu";
            this.tabpageMainMenu.MouseDown += new System.Windows.Forms.MouseEventHandler(this.APIInterrupt_MouseDown);
            // 
            // btnSettings
            // 
            this.btnSettings.BackColor = System.Drawing.Color.Transparent;
            this.btnSettings.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btnSettings.FlatAppearance.BorderSize = 0;
            this.btnSettings.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnSettings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSettings.Font = new System.Drawing.Font("Walkway UltraBold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSettings.ForeColor = System.Drawing.Color.White;
            this.btnSettings.Location = new System.Drawing.Point(26, 300);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(132, 30);
            this.btnSettings.TabIndex = 27;
            this.btnSettings.Text = "settings";
            this.btnSettings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSettings.UseVisualStyleBackColor = false;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // btnStartGame
            // 
            this.btnStartGame.BackColor = System.Drawing.Color.Transparent;
            this.btnStartGame.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btnStartGame.FlatAppearance.BorderSize = 0;
            this.btnStartGame.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnStartGame.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnStartGame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStartGame.Font = new System.Drawing.Font("Walkway UltraBold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartGame.ForeColor = System.Drawing.Color.White;
            this.btnStartGame.Location = new System.Drawing.Point(26, 226);
            this.btnStartGame.Name = "btnStartGame";
            this.btnStartGame.Size = new System.Drawing.Size(132, 30);
            this.btnStartGame.TabIndex = 26;
            this.btnStartGame.Text = "start game";
            this.btnStartGame.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStartGame.UseVisualStyleBackColor = false;
            this.btnStartGame.Click += new System.EventHandler(this.btnStartGameMenu_Click);
            // 
            // btnExitGame
            // 
            this.btnExitGame.BackColor = System.Drawing.Color.Transparent;
            this.btnExitGame.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btnExitGame.FlatAppearance.BorderSize = 0;
            this.btnExitGame.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnExitGame.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnExitGame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExitGame.Font = new System.Drawing.Font("Walkway UltraBold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExitGame.ForeColor = System.Drawing.Color.White;
            this.btnExitGame.Location = new System.Drawing.Point(26, 336);
            this.btnExitGame.Name = "btnExitGame";
            this.btnExitGame.Size = new System.Drawing.Size(132, 35);
            this.btnExitGame.TabIndex = 25;
            this.btnExitGame.Text = "exit game";
            this.btnExitGame.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExitGame.UseVisualStyleBackColor = false;
            this.btnExitGame.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // tabpageStartGame
            // 
            this.tabpageStartGame.BackColor = System.Drawing.Color.Transparent;
            this.tabpageStartGame.BackgroundImage = global::ClientApplication.Properties.Resources.menu_start;
            this.tabpageStartGame.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabpageStartGame.Controls.Add(this.panelStartGameSubmenu);
            this.tabpageStartGame.Controls.Add(this.btnBackStartGame);
            this.tabpageStartGame.Controls.Add(this.btnHost);
            this.tabpageStartGame.Controls.Add(this.btnJoin);
            this.tabpageStartGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabpageStartGame.Location = new System.Drawing.Point(4, 22);
            this.tabpageStartGame.Margin = new System.Windows.Forms.Padding(0);
            this.tabpageStartGame.Name = "tabpageStartGame";
            this.tabpageStartGame.Size = new System.Drawing.Size(605, 401);
            this.tabpageStartGame.TabIndex = 1;
            this.tabpageStartGame.Text = "StartGame";
            this.tabpageStartGame.MouseDown += new System.Windows.Forms.MouseEventHandler(this.APIInterrupt_MouseDown);
            // 
            // panelStartGameSubmenu
            // 
            this.panelStartGameSubmenu.Controls.Add(this.cbMazeSize);
            this.panelStartGameSubmenu.Controls.Add(this.cbGameMode);
            this.panelStartGameSubmenu.Controls.Add(this.cbDifficulty);
            this.panelStartGameSubmenu.Controls.Add(this.cbLevel);
            this.panelStartGameSubmenu.Controls.Add(this.btnBegin);
            this.panelStartGameSubmenu.Controls.Add(this.textBoxIpPort);
            this.panelStartGameSubmenu.Controls.Add(this.labelPortIp);
            this.panelStartGameSubmenu.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelStartGameSubmenu.Location = new System.Drawing.Point(405, 0);
            this.panelStartGameSubmenu.Name = "panelStartGameSubmenu";
            this.panelStartGameSubmenu.Size = new System.Drawing.Size(200, 401);
            this.panelStartGameSubmenu.TabIndex = 32;
            // 
            // cbMazeSize
            // 
            this.cbMazeSize.BackColor = System.Drawing.SystemColors.Window;
            this.cbMazeSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMazeSize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbMazeSize.FormattingEnabled = true;
            this.cbMazeSize.Items.AddRange(new object[] {
            "Tiny",
            "Small",
            "Medium",
            "Large"});
            this.cbMazeSize.Location = new System.Drawing.Point(17, 268);
            this.cbMazeSize.Name = "cbMazeSize";
            this.cbMazeSize.Size = new System.Drawing.Size(174, 25);
            this.cbMazeSize.TabIndex = 34;
            // 
            // cbGameMode
            // 
            this.cbGameMode.BackColor = System.Drawing.SystemColors.Window;
            this.cbGameMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbGameMode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbGameMode.FormattingEnabled = true;
            this.cbGameMode.Items.AddRange(new object[] {
            "Arcade",
            "Classic"});
            this.cbGameMode.Location = new System.Drawing.Point(17, 165);
            this.cbGameMode.Name = "cbGameMode";
            this.cbGameMode.Size = new System.Drawing.Size(174, 25);
            this.cbGameMode.TabIndex = 30;
            this.cbGameMode.SelectedIndexChanged += new System.EventHandler(this.cbGameMode_Changed);
            // 
            // cbDifficulty
            // 
            this.cbDifficulty.BackColor = System.Drawing.SystemColors.Window;
            this.cbDifficulty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDifficulty.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbDifficulty.FormattingEnabled = true;
            this.cbDifficulty.Items.AddRange(new object[] {
            "Easy",
            "Normal",
            "Hard"});
            this.cbDifficulty.Location = new System.Drawing.Point(17, 208);
            this.cbDifficulty.Name = "cbDifficulty";
            this.cbDifficulty.Size = new System.Drawing.Size(174, 25);
            this.cbDifficulty.TabIndex = 31;
            // 
            // cbLevel
            // 
            this.cbLevel.BackColor = System.Drawing.SystemColors.Window;
            this.cbLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLevel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbLevel.FormattingEnabled = true;
            this.cbLevel.Items.AddRange(new object[] {
            "Basic1",
            "Basic2",
            "Basic3",
            "Basic4",
            "Basic5"});
            this.cbLevel.Location = new System.Drawing.Point(17, 268);
            this.cbLevel.Name = "cbLevel";
            this.cbLevel.Size = new System.Drawing.Size(174, 25);
            this.cbLevel.TabIndex = 32;
            // 
            // btnBegin
            // 
            this.btnBegin.BackColor = System.Drawing.Color.Transparent;
            this.btnBegin.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btnBegin.FlatAppearance.BorderSize = 0;
            this.btnBegin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnBegin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnBegin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBegin.Font = new System.Drawing.Font("Walkway UltraBold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBegin.ForeColor = System.Drawing.Color.White;
            this.btnBegin.Location = new System.Drawing.Point(108, 336);
            this.btnBegin.Name = "btnBegin";
            this.btnBegin.Size = new System.Drawing.Size(83, 36);
            this.btnBegin.TabIndex = 33;
            this.btnBegin.Text = "begin";
            this.btnBegin.UseVisualStyleBackColor = false;
            this.btnBegin.Click += new System.EventHandler(this.btnStartGame_Click);
            // 
            // textBoxIpPort
            // 
            this.textBoxIpPort.BackColor = System.Drawing.Color.White;
            this.textBoxIpPort.Location = new System.Drawing.Point(17, 59);
            this.textBoxIpPort.Name = "textBoxIpPort";
            this.textBoxIpPort.Size = new System.Drawing.Size(174, 23);
            this.textBoxIpPort.TabIndex = 29;
            // 
            // labelPortIp
            // 
            this.labelPortIp.AutoSize = true;
            this.labelPortIp.BackColor = System.Drawing.Color.Transparent;
            this.labelPortIp.Font = new System.Drawing.Font("Walkway UltraBold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPortIp.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelPortIp.Location = new System.Drawing.Point(14, 35);
            this.labelPortIp.Name = "labelPortIp";
            this.labelPortIp.Size = new System.Drawing.Size(58, 17);
            this.labelPortIp.TabIndex = 30;
            this.labelPortIp.Text = "on port";
            // 
            // btnBackStartGame
            // 
            this.btnBackStartGame.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBackStartGame.BackColor = System.Drawing.Color.Transparent;
            this.btnBackStartGame.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btnBackStartGame.FlatAppearance.BorderSize = 0;
            this.btnBackStartGame.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnBackStartGame.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnBackStartGame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBackStartGame.Font = new System.Drawing.Font("Walkway UltraBold", 14.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBackStartGame.ForeColor = System.Drawing.Color.White;
            this.btnBackStartGame.Location = new System.Drawing.Point(26, 336);
            this.btnBackStartGame.Name = "btnBackStartGame";
            this.btnBackStartGame.Size = new System.Drawing.Size(132, 35);
            this.btnBackStartGame.TabIndex = 26;
            this.btnBackStartGame.Text = "back";
            this.btnBackStartGame.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBackStartGame.UseVisualStyleBackColor = false;
            this.btnBackStartGame.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnHost
            // 
            this.btnHost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnHost.BackColor = System.Drawing.Color.Transparent;
            this.btnHost.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btnHost.FlatAppearance.BorderSize = 0;
            this.btnHost.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnHost.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnHost.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHost.Font = new System.Drawing.Font("Walkway UltraBold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHost.ForeColor = System.Drawing.Color.White;
            this.btnHost.Location = new System.Drawing.Point(23, 246);
            this.btnHost.Name = "btnHost";
            this.btnHost.Size = new System.Drawing.Size(132, 30);
            this.btnHost.TabIndex = 27;
            this.btnHost.Text = "host game";
            this.btnHost.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHost.UseVisualStyleBackColor = false;
            this.btnHost.Click += new System.EventHandler(this.btnHost_Click);
            // 
            // btnJoin
            // 
            this.btnJoin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnJoin.BackColor = System.Drawing.Color.Transparent;
            this.btnJoin.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btnJoin.FlatAppearance.BorderSize = 0;
            this.btnJoin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnJoin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnJoin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnJoin.Font = new System.Drawing.Font("Walkway UltraBold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnJoin.ForeColor = System.Drawing.Color.White;
            this.btnJoin.Location = new System.Drawing.Point(23, 282);
            this.btnJoin.Name = "btnJoin";
            this.btnJoin.Size = new System.Drawing.Size(132, 30);
            this.btnJoin.TabIndex = 28;
            this.btnJoin.Text = "join game";
            this.btnJoin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnJoin.UseVisualStyleBackColor = false;
            this.btnJoin.Click += new System.EventHandler(this.btnJoin_Click);
            // 
            // tabpageSettings
            // 
            this.tabpageSettings.BackColor = System.Drawing.Color.Transparent;
            this.tabpageSettings.BackgroundImage = global::ClientApplication.Properties.Resources.menu_screen;
            this.tabpageSettings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabpageSettings.Controls.Add(this.cbThemeSelect);
            this.tabpageSettings.Controls.Add(this.btnPlayMusic);
            this.tabpageSettings.Controls.Add(this.pbMusicOnOff);
            this.tabpageSettings.Controls.Add(this.btnBackSettings);
            this.tabpageSettings.Location = new System.Drawing.Point(4, 22);
            this.tabpageSettings.Margin = new System.Windows.Forms.Padding(0);
            this.tabpageSettings.Name = "tabpageSettings";
            this.tabpageSettings.Size = new System.Drawing.Size(605, 401);
            this.tabpageSettings.TabIndex = 2;
            this.tabpageSettings.Text = "Settings";
            this.tabpageSettings.MouseDown += new System.Windows.Forms.MouseEventHandler(this.APIInterrupt_MouseDown);
            // 
            // cbThemeSelect
            // 
            this.cbThemeSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbThemeSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbThemeSelect.FormattingEnabled = true;
            this.cbThemeSelect.Items.AddRange(new object[] {
            "Default",
            "Mukxa",
            "Retro",
            "Sky"});
            this.cbThemeSelect.Location = new System.Drawing.Point(209, 234);
            this.cbThemeSelect.Name = "cbThemeSelect";
            this.cbThemeSelect.Size = new System.Drawing.Size(132, 21);
            this.cbThemeSelect.TabIndex = 31;
            this.cbThemeSelect.SelectedIndexChanged += new System.EventHandler(this.cbUserTheme_Changed);
            // 
            // btnPlayMusic
            // 
            this.btnPlayMusic.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPlayMusic.BackColor = System.Drawing.Color.Transparent;
            this.btnPlayMusic.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btnPlayMusic.FlatAppearance.BorderSize = 0;
            this.btnPlayMusic.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnPlayMusic.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnPlayMusic.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlayMusic.Font = new System.Drawing.Font("Walkway UltraBold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlayMusic.ForeColor = System.Drawing.Color.White;
            this.btnPlayMusic.Location = new System.Drawing.Point(71, 226);
            this.btnPlayMusic.Name = "btnPlayMusic";
            this.btnPlayMusic.Size = new System.Drawing.Size(132, 30);
            this.btnPlayMusic.TabIndex = 30;
            this.btnPlayMusic.Text = "music";
            this.btnPlayMusic.UseVisualStyleBackColor = false;
            this.btnPlayMusic.Click += new System.EventHandler(this.btnMusicOnOff_Click);
            // 
            // pbMusicOnOff
            // 
            this.pbMusicOnOff.BackColor = System.Drawing.Color.Transparent;
            this.pbMusicOnOff.Image = global::ClientApplication.Properties.Resources.music_ona;
            this.pbMusicOnOff.Location = new System.Drawing.Point(35, 226);
            this.pbMusicOnOff.Name = "pbMusicOnOff";
            this.pbMusicOnOff.Size = new System.Drawing.Size(30, 30);
            this.pbMusicOnOff.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbMusicOnOff.TabIndex = 29;
            this.pbMusicOnOff.TabStop = false;
            // 
            // btnBackSettings
            // 
            this.btnBackSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBackSettings.BackColor = System.Drawing.Color.Transparent;
            this.btnBackSettings.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btnBackSettings.FlatAppearance.BorderSize = 0;
            this.btnBackSettings.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnBackSettings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnBackSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBackSettings.Font = new System.Drawing.Font("Walkway UltraBold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBackSettings.ForeColor = System.Drawing.Color.White;
            this.btnBackSettings.Location = new System.Drawing.Point(26, 336);
            this.btnBackSettings.Name = "btnBackSettings";
            this.btnBackSettings.Size = new System.Drawing.Size(132, 35);
            this.btnBackSettings.TabIndex = 27;
            this.btnBackSettings.Text = "back";
            this.btnBackSettings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBackSettings.UseVisualStyleBackColor = false;
            this.btnBackSettings.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // FormLauncher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(605, 398);
            this.Controls.Add(this.tabContainer);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormLauncher";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Launcher";
            this.tabContainer.ResumeLayout(false);
            this.tabpageMainMenu.ResumeLayout(false);
            this.tabpageStartGame.ResumeLayout(false);
            this.panelStartGameSubmenu.ResumeLayout(false);
            this.panelStartGameSubmenu.PerformLayout();
            this.tabpageSettings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbMusicOnOff)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabContainer;
        private System.Windows.Forms.TabPage tabpageMainMenu;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.Button btnStartGame;
        private System.Windows.Forms.Button btnExitGame;
        private System.Windows.Forms.TabPage tabpageStartGame;
        private System.Windows.Forms.TabPage tabpageSettings;
        private System.Windows.Forms.Button btnBackStartGame;
        private System.Windows.Forms.Button btnBackSettings;
        private System.Windows.Forms.Button btnBegin;
        private System.Windows.Forms.Label labelPortIp;
        private System.Windows.Forms.TextBox textBoxIpPort;
        private System.Windows.Forms.Button btnHost;
        private System.Windows.Forms.Button btnJoin;
        private System.Windows.Forms.PictureBox pbMusicOnOff;
        private System.Windows.Forms.Panel panelStartGameSubmenu;
        private System.Windows.Forms.ComboBox cbLevel;
        private System.Windows.Forms.ComboBox cbGameMode;
        private System.Windows.Forms.ComboBox cbDifficulty;
        private System.Windows.Forms.Button btnPlayMusic;
        private System.Windows.Forms.ComboBox cbThemeSelect;
        private System.Windows.Forms.ComboBox cbMazeSize;
    }
}

