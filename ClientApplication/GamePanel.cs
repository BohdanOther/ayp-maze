﻿using GameService;
using Network.Client;
using Network.Server;
using System;
using System.Drawing;
using System.Media;
using System.Net;
using System.Windows.Forms;

namespace ClientApplication
{
    public partial class GamePanel : Form
    {
        private SoundPlayer soundPlayer;

        private Graphics gGameScreen;

        private Game game;
        private GameServer server;
        private GameClient client;

        public GamePanel()
        {
            InitializeComponent();
            gGameScreen = picGameScreen.CreateGraphics();

            this.picGameScreen.MouseMove += GamePanel_MouseMove;
            this.picGameScreen.MouseDown += picGameScreen_MouseDown;
            this.picGameScreen.MouseUp += picGameScreen_MouseUp;

            soundPlayer = new System.Media.SoundPlayer(Properties.Resources.game_ambient);

            this.SetVolume(5);
            GameService.GameContext.UserTheme = Properties.Settings.Default.UserTheme;
        }

        public void StartGame(bool isServer, IPEndPoint arg, GameParameters gparams)
        {
            game = new Game(gGameScreen, new Size(picGameScreen.Width, picGameScreen.Height), gparams);

            if (isServer)
            {
                this.Text = "Server";
                server = new GameServer(game, arg);
                server.Start();
            }
            else
            {
                this.Text = "Client";
                client = new GameClient(game, arg);
                client.Connect();
            }

            if (Properties.Settings.Default.UserSound)
            {
                soundPlayer.PlayLooping();
            }
        }

        public void StopGame()
        {
            game.Stop();
            soundPlayer.Stop();
        }

        # region Form Handlers

        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.SetVolume(5);
            this.Close();
        }

        private void picGameScreen_MouseUp(object sender, MouseEventArgs e)
        {
            game.inputListener.LMBPressed = false;
        }

        private void picGameScreen_MouseDown(object sender, MouseEventArgs e)
        {
            game.inputListener.LMBPressed = true;
        }

        private void GamePanel_FormClosing(object sender, FormClosingEventArgs e)
        {
            StopGame();
            this.Owner.Show();
            this.Owner.BringToFront();
            this.Owner.Refresh();
        }

        private void GamePanel_MouseMove(object sender, MouseEventArgs e)
        {
            game.inputListener.MousePoint.X = e.X;
            game.inputListener.MousePoint.Y = e.Y;
        }

        private void GamePanel_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    this.Close();
                    break;

                case Keys.F4:
                    GameService.GameContext.ShowFps = !(bool)GameService.GameContext.ShowFps;
                    break;

                case Keys.F5:
                    Game.DebugMode = !(bool)Game.DebugMode;
                    break;

                case Keys.Add:
                    if (this.GetVolume() < 10)
                        this.SetVolume(GetVolume() + 1);
                    break;

                case Keys.Subtract:
                    if (this.GetVolume() > 0)
                        this.SetVolume(GetVolume() - 1);
                    break;

                default: break;
            }
        }

        private void APIInterrupt_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                NativeMethods.ReleaseCapture();
                NativeMethods.SendMessage(this.Handle, NativeMethods.WM_NCLBUTTONDOWN, NativeMethods.HT_CAPTION, 0);
            }
        }

        private void SetVolume(int value)
        {
            // Calculate the volume that's being set
            int NewVolume = ((ushort.MaxValue / 10) * value);

            // Set the same volume for both the left and the right channels
            uint NewVolumeAllChannels = (((uint)NewVolume & 0x0000ffff) | ((uint)NewVolume << 16));

            // Set the volume
            NativeMethods.waveOutSetVolume(IntPtr.Zero, NewVolumeAllChannels);
        }

        private int GetVolume()
        {
            uint CurrVol = 0;
            // At this point, CurrVol gets assigned the volume
            NativeMethods.waveOutGetVolume(IntPtr.Zero, out CurrVol);

            // Calculate the volume
            ushort CalcVol = (ushort)(CurrVol & 0x0000ffff);

            // Get the volume on a scale of 1 to 10 (to fit the trackbar)
            return CalcVol / (ushort.MaxValue / 10);
        }

        # endregion
    }
}
