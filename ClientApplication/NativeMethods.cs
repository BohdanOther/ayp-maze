﻿using System;
using System.Runtime.InteropServices;

namespace ClientApplication
{
    internal static class NativeMethods
    {
        internal const int WM_NCLBUTTONDOWN = 0xA1;
        internal const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        internal static extern IntPtr SendMessage(IntPtr hWnd, int Msg, uint wParam, uint lParam);

        [DllImportAttribute("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool ReleaseCapture();

        [DllImport("winmm.dll")]
        internal static extern IntPtr waveOutGetVolume(IntPtr hwo, out uint dwVolume);

        [DllImport("winmm.dll")]
        internal static extern IntPtr waveOutSetVolume(IntPtr hwo, uint dwVolume);
    }
}
