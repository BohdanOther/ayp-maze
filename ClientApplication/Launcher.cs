﻿using System;
using System.Drawing;
using System.Drawing.Text;
using System.Media;
using System.Net;
using System.Windows.Forms;

namespace ClientApplication
{
    public partial class FormLauncher : Form
    {
        private PrivateFontCollection pfc;
        private SoundPlayer soundPlayer;
        private GamePanel frmGamePanel;

        private bool isServer;

        public FormLauncher()
        {
            InitializeComponent();
            // CUSTOM FONT
            pfc = new PrivateFontCollection();
            String path = GameService.GameContext.ResourcesPath + @"Walkway_UltraBold.ttf";
            pfc.AddFontFile(path);

            Font regFont = new Font(
            pfc.Families[0],
            14.25f,
            FontStyle.Regular,
            GraphicsUnit.Point);

            btnExitGame.Font = regFont;
            btnHost.Font = regFont;
            btnJoin.Font = regFont;
            btnBegin.Font = regFont;
            btnStartGame.Font = regFont;
            btnSettings.Font = regFont;
            btnPlayMusic.Font = regFont;
            btnBackStartGame.Font = regFont;
            btnBackSettings.Font = regFont;
            labelPortIp.Font = regFont;

            // BACKGROUNG MUSIC
            soundPlayer = new System.Media.SoundPlayer(Properties.Resources.Onairstudio_Gravity_LauncherBackground);
            if (Properties.Settings.Default.UserSound)
            {
                soundPlayer.PlayLooping();
                pbMusicOnOff.Image = Properties.Resources.music_ona;
            }
            else
            {
                pbMusicOnOff.Image = Properties.Resources.music_off;
            }

            // init
            cbGameMode.SelectedIndex = 1;
            cbDifficulty.SelectedIndex = 0;
            cbLevel.SelectedIndex = 0;
            cbThemeSelect.SelectedIndex = Properties.Settings.Default.cbThemeIndex;
            cbMazeSize.SelectedIndex = 0;
        }

        #region Form Events Handling

        private void btnExit_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
            this.Close();
            Environment.Exit(0);
        }

        private void btnStartGame_Click(object sender, EventArgs e)
        {
            uint userMazeSize = 3;

            switch (cbMazeSize.SelectedItem.ToString())
            {
                case "Tiny": userMazeSize = 21; break;
                case "Small": userMazeSize = 41; break;
                case "Medium": userMazeSize = 81; break;
                case "Large": userMazeSize = 121; break;
            }

            GameService.GameParameters gparams = new GameService.GameParameters()
            {
                Difficulty = (GameService.Difficulty)Enum.Parse(typeof(GameService.Difficulty), cbDifficulty.SelectedItem.ToString()),
                GameMode = (GameService.GameMode)Enum.Parse(typeof(GameService.GameMode), cbGameMode.SelectedItem.ToString()),
                LevelName = cbLevel.SelectedItem.ToString(),
                MazeSize = userMazeSize
            };

            if (isServer)
            {
                // true - we're gona be server
                int port;
                if (!int.TryParse(textBoxIpPort.Text, out port))
                {
                    MessageBox.Show("Enter port!");
                    return;
                }
                IPEndPoint address = new IPEndPoint(0, port);

                frmGamePanel = new GamePanel();

                // start waiting for clients
                this.Hide();
                soundPlayer.Stop();
                frmGamePanel.Show(this);
                frmGamePanel.BringToFront();
                frmGamePanel.StartGame(true, address, gparams);
            }
            else // we're client
            {
                IPEndPoint address = null;
                try
                {
                    string data = textBoxIpPort.Text;
                    string portData = data.Substring(data.IndexOf(':') + 1, data.Length - data.IndexOf(':') - 1);
                    int port = int.Parse(portData);
                    IPAddress ip = IPAddress.Parse(data.Substring(0, data.IndexOf(':')));

                    address = new IPEndPoint(ip, port);
                }
                catch
                {
                    MessageBox.Show("Wrong address: make it like : ip:port");
                    return;
                }

                frmGamePanel = new GamePanel();
                this.Hide();
                soundPlayer.Stop();
                frmGamePanel.Show(this);
                frmGamePanel.BringToFront();

                // false - we're gonna be client
                frmGamePanel.StartGame(false, address, gparams);
            }
        }

        private void btnHost_Click(object sender, EventArgs e)
        {
            btnJoin.ForeColor = Color.White;
            btnHost.ForeColor = Color.Coral;
            labelPortIp.Text = "on port";
            textBoxIpPort.Text = "5555";
            textBoxIpPort.Focus();
            isServer = true;
            cbGameMode.Visible = true;
            cbGameMode.SelectedIndex = 1;
            cbDifficulty.Visible = true;
            cbMazeSize.Visible = true;
        }

        private void btnJoin_Click(object sender, EventArgs e)
        {
            btnJoin.ForeColor = Color.Coral;
            btnHost.ForeColor = Color.White;
            labelPortIp.Text = "ip : port";
            textBoxIpPort.Text = "127.0.0.1:5555";
            textBoxIpPort.Focus();
            isServer = false;
            cbGameMode.Visible = false;
            cbLevel.Visible = false;
            cbDifficulty.Visible = false;
            cbMazeSize.Visible = false;
        }

        private void btnMusicOnOff_Click(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UserSound)
            {
                pbMusicOnOff.Image = Properties.Resources.music_off;
                Properties.Settings.Default.UserSound = false;
                soundPlayer.Stop();
            }
            else
            {
                pbMusicOnOff.Image = Properties.Resources.music_ona;
                Properties.Settings.Default.UserSound = true;
                soundPlayer.PlayLooping();
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            tabContainer.SelectedIndex = 0;
        }

        private void btnStartGameMenu_Click(object sender, EventArgs e)
        {
            tabContainer.SelectedIndex = 1;
            btnHost.PerformClick();
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            tabContainer.SelectedIndex = 2;
        }

        private void cbGameMode_Changed(object sender, EventArgs e)
        {
            string item = cbGameMode.SelectedItem.ToString();

            if (item.Equals("Arcade"))
            {
                cbLevel.Visible = true;
                cbMazeSize.Visible = false;
            }
            else
            {
                cbMazeSize.Visible = true;
                cbLevel.Visible = false;
            }
        }

        private void cbUserTheme_Changed(object sender, EventArgs e)
        {
            Properties.Settings.Default.UserTheme = cbThemeSelect.SelectedItem.ToString();
            Properties.Settings.Default.cbThemeIndex = cbThemeSelect.SelectedIndex;
        }

        private void APIInterrupt_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                NativeMethods.ReleaseCapture();
                NativeMethods.SendMessage(Handle, NativeMethods.WM_NCLBUTTONDOWN, NativeMethods.HT_CAPTION, 0);
            }
        }

        #endregion
    }
}
