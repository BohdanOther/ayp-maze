# README #

[Youtube demonstration](https://www.youtube.com/watch?v=YLkdcX_aahw)

### What is this repository for? ###

* Quick summary
This is  the academic year project. Base concept is follwing :  labyrinth with client/server functionality. So you'll get some maze-roaming with your friends via network

* Version 0.0.1 SNAPSHOT

### How do I get set up? ###

* clone repository
* build project
* **run**  ./ayp-maze/ClientApplication/bin/Debug/ClientApplication.exe
* host game
* **run** client again and join created local game;
* have fun
// you might need to allow app to use networking
* you can look at little demo [here](https://www.youtube.com/watch?v=YLkdcX_aahw)