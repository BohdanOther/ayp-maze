﻿namespace Network.Client
{
    public enum ClientState
    {
        None,
        Connected,
        Disconnected,
        ConnectionLost
    }
}
