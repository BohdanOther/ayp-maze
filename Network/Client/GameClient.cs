﻿using GameService;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;

namespace Network.Client
{
    public sealed class GameClient : IDisposable
    {
        private Game game;

        public ClientState ClientState { get; set; }

        private IPEndPoint ipEndPoint;
        private TcpClient client;

        public GameClient(Game game, IPEndPoint arg)
        {
            this.game = game;

            ipEndPoint = arg;
            client = new TcpClient();
        }

        public void Connect()
        {
            client.Connect(ipEndPoint.Address, ipEndPoint.Port);

            UpdateState(ClientState.Connected);
            Thread t = new Thread(() => HandleConnection(client));
            t.Start();
        }

        public void HandleConnection(TcpClient client)
        {
            using (StreamWriter sWriter = new StreamWriter(client.GetStream(), Encoding.ASCII))
            using (StreamReader sReader = new StreamReader(client.GetStream(), Encoding.ASCII))
            {
                // read game params from server
                GameContext.GameParameters = ReciveGameParameters(client);
                sWriter.WriteLine("Ok!");

                if (GameContext.GameParameters.GameMode == GameMode.Arcade)
                {
                    game.world.LoadLevel(GameContext.GameParameters.LevelName);
                }
                else
                {
                    game.world.GenerateLevelFromRawMap(GameContext.GameParameters.GeneratedLevel);
                }

                game.Start();

                while (ClientState == Client.ClientState.Connected)
                {
                    try
                    {
                        //
                        // send response
                        //
                        sWriter.WriteLine(GameContext.RemoteContext.LocalX.ToString());
                        sWriter.WriteLine(GameContext.RemoteContext.LocalY.ToString());
                        sWriter.WriteLine(GameContext.RemoteContext.LocalFinishReached.ToString());
                        sWriter.Flush();
                        //
                        // read income
                        //
                        GameContext.RemoteContext.RemoteX = Int32.Parse(sReader.ReadLine());
                        GameContext.RemoteContext.RemoteY = Int32.Parse(sReader.ReadLine());
                        GameContext.RemoteContext.RemoteFinishReached = Boolean.Parse(sReader.ReadLine());
                    }
                    catch
                    {
                        Game.ShowNotification("Connection Lost", 400);
                        Game.ShowNotification("You're offline...", 400);
                        UpdateState(ClientState.ConnectionLost);
                    }
                }
            }
        }

        private GameParameters ReciveGameParameters(TcpClient client)
        {
            GameParameters param;

            byte[] readMsgLen = new byte[4];
            client.GetStream().Read(readMsgLen, 0, 4);

            int dataLen = BitConverter.ToInt32(readMsgLen, 0);
            byte[] readMsgData = new byte[dataLen];
            client.GetStream().Read(readMsgData, 0, dataLen);

            using (MemoryStream ms = new MemoryStream(readMsgData))
            {
                BinaryFormatter bf = new BinaryFormatter();
                param = (GameParameters)bf.Deserialize(ms);
            }
            return param;
        }

        public void Disconnect()
        {
            if (ClientState == ClientState.Connected)
            {
                UpdateState(ClientState.Disconnected);
            }
        }

        public void UpdateState(ClientState newState)
        {
            ClientState = newState;
        }

        public void Dispose()
        {
            client.Close();
        }
    }
}
