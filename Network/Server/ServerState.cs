﻿namespace Network.Server
{
    public enum ServerState
    {
        None,
        Stopped,
        Running,
        ConnectionLost,
        WaitingForClients
    }
}
