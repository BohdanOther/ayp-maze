﻿using GameService;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;

namespace Network.Server
{
    public class GameServer
    {
        private Game game;

        public ServerState State { get; set; }

        private TcpListener server;

        public GameServer(Game game, IPEndPoint arg)
        {
            server = new TcpListener(IPAddress.Any, arg.Port);
            this.game = game;

            if (GameContext.GameParameters.GameMode == GameMode.Arcade)
            {
                game.world.LoadLevel(GameContext.GameParameters.LevelName);
            }
            else
            {
                game.world.GenerateLevel();
            }
        }

        public void Start()
        {
            game.Start();
            game.ShowGlobalNotification("Waiting for clients");
            UpdateState(ServerState.WaitingForClients);
            //LoopClients();
            game.HideGlobalNotification();
        }

        public void Stop()
        {
            UpdateState(ServerState.Stopped);
        }

        private void LoopClients()
        {
            int clientCount = 0;

            server.Start();
            while (clientCount <= 0)
            {
                TcpClient newClient = server.AcceptTcpClient();

                Thread t = new Thread(() => HandleClient(newClient));
                UpdateState(ServerState.Running);
                clientCount++;
                t.Start();
            }
            server.Stop();
        }

        private void HandleClient(TcpClient client)
        {
            using (StreamWriter sWriter = new StreamWriter(client.GetStream(), Encoding.ASCII))
            using (StreamReader sReader = new StreamReader(client.GetStream(), Encoding.ASCII))
            {
                // send game parameters
                SendGameParameters(GameContext.GameParameters, client);

                // wait for "roger that"
                sReader.ReadLine();

                while (State == ServerState.Running)
                {
                    try
                    {
                        //
                        // read income
                        //
                        GameContext.RemoteContext.RemoteX = Int32.Parse(sReader.ReadLine());
                        GameContext.RemoteContext.RemoteY = Int32.Parse(sReader.ReadLine());
                        GameContext.RemoteContext.RemoteFinishReached = Boolean.Parse(sReader.ReadLine());
                        //
                        // send responce
                        //
                        sWriter.WriteLine(GameContext.RemoteContext.LocalX.ToString());
                        sWriter.WriteLine(GameContext.RemoteContext.LocalY.ToString());
                        sWriter.WriteLine(GameContext.RemoteContext.LocalFinishReached.ToString());
                        sWriter.Flush();
                    }
                    catch
                    {
                        Game.ShowNotification("Client disconected", 400);
                        UpdateState(ServerState.ConnectionLost);
                        sWriter.Close();
                        sReader.Close();
                        break;
                    }
                }
            }
        }

        private void SendGameParameters(GameParameters param, TcpClient client)
        {
            byte[] gameParametersSerialized;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, GameContext.GameParameters);
                gameParametersSerialized = ms.ToArray();
            }

            byte[] messageLength = BitConverter.GetBytes((Int32)gameParametersSerialized.Length);
            // WRITE MESSAGE LENGTH
            client.GetStream().Write(messageLength, 0, 4);
            // WRITE MESSAGE
            client.GetStream().Write(gameParametersSerialized, 0, gameParametersSerialized.Length);
        }

        public void UpdateState(ServerState newState)
        {
            State = newState;
        }
    }
}
